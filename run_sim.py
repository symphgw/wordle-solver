#!/usr/local/bin/python3

import wordle_game
import wordle_solver
import sys
import math
import csv
from distutils.util import strtobool

def runGameAndSolve(game, solver) :
	outcomes = []
	while (len(outcomes) == 0 or not game.isSolved()) :
		# print('---------- Guess', len(outcomes)+1, '----------')
		past_guesses = game.getState()
		# print('Past guesses:', past_guesses)
		pre_n_possible_answers = len(solver.getPossibleAnswers(past_guesses))
		# print('Possible Answers', pre_n_possible_answers)
		next_guess = solver.getNextGuess(past_guesses)
		# next_guess = 'logoi'
		# print('Making guess:', next_guess)
		outcome = game.makeGuess(next_guess)
		# print('Outcome:', outcome)
		outcomes.append((pre_n_possible_answers, next_guess, outcome))

	# print('-------------------------')
	# print('Answer is', next_guess)
	# print('Solved in', len(outcomes), 'guesses')	
	print(outcomes)

	return (solver.getStrategy()[0], solver.getStrategy()[1], outcomes)
	# return (outcomes)

def runGames(size, rank_bys, valid_guesses_onlys, is_randomised) :
	solver = wordle_solver.WordleSolver()
	game = wordle_game.WordleGame(is_randomised)
	game_outcomes = []
	n_game = 0
	for rank_by in rank_bys :
		for valid_guesses_only in valid_guesses_onlys :
			solver.setStrategy(rank_by, valid_guesses_only)
			game.resetGame()
			for i in range(size) :
				print('==============================')
				print('Game', n_game+1, rank_by, valid_guesses_only)
				# print('==============================')
				game.nextGame()
				completed_game = runGameAndSolve(game, solver)
				game_outcomes.append(completed_game)
				n_game += 1

	print('==============================')
	# print(game_outcomes)
	print()
	return game_outcomes


def write_to_csv(sim_res) :
	
	game_file_name = 'games.csv'
	game_file = open(game_file_name, 'w', newline='')
	game_file_writer = csv.writer(game_file, quoting=csv.QUOTE_NONNUMERIC)
	games_header = ['game_id', 'rank_by', 'valid_guesses_only', 'n_guesses_to_solve', 'answer']
	game_file_writer.writerow(games_header)

	guess_file_name = 'guesses.csv'
	guess_file = open(guess_file_name, 'w', newline='')
	guess_file_writer = csv.writer(guess_file, quoting=csv.QUOTE_NONNUMERIC)
	guess_header = ['game_id', 'guess_id', 'guess_number', 'rank_by', 'valid_guesses_only', 'n_possible_answers', 'guess', 'outcome']
	guess_file_writer.writerow(guess_header)

	for i, game in enumerate(sim_res) :
		rank_by = game[0]
		valid_guesses_only = game[1]
		guesses = game[2]
		n_guesses = len(guesses)
		game_file_writer.writerow([i, rank_by, valid_guesses_only, n_guesses, guesses[n_guesses-1][1]])

		for j, guess in enumerate(guesses) :
			guess_file_writer.writerow([i, int(str(i)+str(j)), j, rank_by, valid_guesses_only, guess[0], guess[1], guess[2]])

	game_file.close
	guess_file.close


if len(sys.argv) == 1 :
	runGames(1, ['find_letter'], [True], True)
	exit()

size = int(sys.argv[1])
# rank_bys = ['None', 'find_letter', 'find_pos']
rank_bys = ['none']
# valid_guesses_onlys = [False, True]
valid_guesses_onlys = [False]
is_randomised = True
sim_res = runGames(size, rank_bys, valid_guesses_onlys, is_randomised)
write_to_csv(sim_res)