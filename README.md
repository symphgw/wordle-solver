# README #

### About ###

* A collection of Python scripts which tries to suggest an optimal guess for solving the Wordle game (https://www.powerlanguage.co.uk/wordle/)

### Solver strategy & variation ###

* `rank_by='find_pos'`  - Eliminate possible answers by overall letter frequency
* `rank_by='find_letter'` - Eliminate possible answers by per position letter frequency
* `rank_by='none'` - Random guesses
* `valid_guesses_only=false` - any words that have not been used can be used as a guess
* `valid_guesses_only=true` - only make guesses that are valid answers

### Usage ###

#### Manual solver

* Open `manual_solver.py`
* Enter past guesses and outcomes
* Set strategy and variation for solver
* `./manual_solver.py`

#### Run solver against a single generated games

* Open `run_sim.py`
* Set strategy and variation for solver
* `./run_sim.py`

#### Run solver against a multiple generated games

* Open `run_sim.py`
* Set strategy and variation for solver
* `./run_sim.py <number of games>`
* Each strategy and variation 
* Outcomes will be written to `games.csv` and `guesses.csv`
