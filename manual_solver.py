#!/usr/local/bin/python3

import wordle_solver

past_guesses = []
# past_guesses.append(('jaunt', 'XXXYX'))
# past_guesses.append(('arose', 'XXGXX'))
# past_guesses.append(('groin', 'XXGXY'))
# past_guesses.append(('nooky', 'YXGYX'))
# past_guesses.append(('soare', 'XYXXX'))
# past_guesses.append(('until', 'XGXXG'))
# past_guesses.append(('indol', 'XGXYG'))
# past_guesses.append(('cares', 'XXXXX'))
# past_guesses.append(('soote', 'XXGXX'))
# past_guesses.append(('booty', 'XXGXX'))

solver = wordle_solver.WordleSolver()
solver.setStrategy('find_pos', True)
print(len(solver.getPossibleAnswers(past_guesses)))
print(solver.getNextGuess(past_guesses))