#!/usr/local/bin/python3

from random import randrange
from wordle_dict import wordle_dict

class WordleGame :

	def resetGame(self) :
		self.game_count = 0
		self.answer = ""
		self.guesses = []
		self.outcomes = []

	def nextGame(self) :
		if self.is_randomised == True :
			i = randrange(0, len(wordle_dict))
		else :
			i = self.game_count
		self.answer = wordle_dict[i].lower()
		self.answer = "vares"
		self.game_count += 1
		self.guesses = []
		self.outcomes = []
		print('New game: ', self.answer)

	def getGameCount(self) :
		return self.game_count

	def getState(self) :
		state = []
		for guess, outcome in zip(self.guesses, self.outcomes) :
			state.append((guess, outcome))
		return state

	def makeGuess(self, guess) :
		guess = guess.strip().lower()
		if not (isinstance(guess, str)
			and len(guess) == 5 
			and guess.isalpha()
			and guess in wordle_dict) :
			print("Invalid guess", guess)
			return
		
		self.guesses.append(guess)

		compare = self.answer
		outcome = ""
		for i, letter in enumerate(guess) :
			if self.answer[i] == letter :
				outcome += 'G'
				compare = compare.replace(letter, '-', 1)
			elif letter in compare :
				outcome += 'Y'
				compare = compare.replace(letter, '-', 1)
			else :
				outcome += 'X'
			# print(compare)

		self.outcomes.append(outcome)
		# print(guess, outcome)
		
		# return self.getState()
		return outcome

	def isSolved(self) :
		for guess in self.getState() :
			if guess[1] == 'GGGGG' :
				return True
		return False

	def __init__(self, is_randomised) :
		self.game_count = 0
		self.is_randomised = is_randomised
		self.answer = ""
		self.guesses = []
		self.outcomes = []
		self.resetGame()

