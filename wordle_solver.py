#!/usr/local/bin/python3

from wordle_dict import five_letter_words
from wordle_dict import fill_freq_maps
from wordle_dict import word_letter_freq_ranked
from wordle_dict import pos_letter_freq_ranked
from operator import itemgetter
from random import randrange

class WordleSolver :

	class InfoMap :
		def __init__(self, tried_words, tried_letters, confirmed_letters, exclude_letters, letter_pos, pos_letter) :
			self.tried_words = tried_words
			self.tried_letters = tried_letters
			self.confirmed_letters = confirmed_letters
			self.exclude_letters = exclude_letters
			self.letter_pos = letter_pos
			self.pos_letter = pos_letter


	def updateInfoMap(self, past_outcomes) :
		# make info map of known letters and possible positions
		tried_words = []
		tried_letters = set()
		confirmed_letters = set()
		exclude_letters = set()
		letter_pos = {}
		pos_letter = {}
		for tri in past_outcomes :
			tried_words.append(tri[0])
			tried_letters |= set(tri[0])
			for pos, colour in enumerate(tri[1]) :
				letter = tri[0][pos]
				if colour == 'Y' :
					confirmed_letters.add(letter)
					if letter not in letter_pos :
						letter_pos[letter] = list(range(5))
					if pos in letter_pos[letter] :
						letter_pos[letter].remove(pos)
				elif colour == 'G' :
					confirmed_letters.add(letter)
					pos_letter[pos] = letter
		exclude_letters = tried_letters - confirmed_letters

		# print(tried_letters)
		# print(confirmed_letters)
		# print(exclude_letters)
		# print(letter_pos)
		# print(pos_letter)

		self.info_map = self.InfoMap(tried_words, tried_letters, confirmed_letters, exclude_letters, letter_pos, pos_letter)

		return self.info_map


	def isValidAnswer(self, word, info_map) :
		if word in info_map.tried_words :
			return False
		
		match = True
		found_confirmed_letters = set()
		for i, letter in enumerate(word) :
			# has excluded letters
			if letter in info_map.exclude_letters :
				match = False
				# print(word, letter, 'is excluded')
				break
			# has wrong letter in a known spot
			if i in info_map.pos_letter and info_map.pos_letter[i] != letter :
				match = False
				# print(word, i, letter, 'is not', pos_letter[i])
				break
			# note if letter is in known set
			if letter in info_map.confirmed_letters :
				found_confirmed_letters.add(letter)
				# check position possible
				if (letter in info_map.letter_pos and i not in info_map.letter_pos[letter]) :
					match = False
					# print(word, letter, 'not in pos', letter_pos[letter])
					break
		
		# word contains all confirmed letters
		if info_map.confirmed_letters != found_confirmed_letters :
			match = False
			# print(word, confirmed_letters, found_confirmed_letters, 'mismatch')
		return match


	def getPossibleAnswers(self, past_guesses_with_outcome) :
		info_map = self.updateInfoMap(past_guesses_with_outcome)
		possible_answers = []
		for word in five_letter_words :
			if self.isValidAnswer(word, info_map) :
				possible_answers.append(word)

		# print('Possile answers:', len(possible_answers))
		# print(possible_answers)

		return possible_answers


	def getNextGuess(self, past_guesses_with_outcome) :
		possible_answers = self.getPossibleAnswers(past_guesses_with_outcome)
		if len(possible_answers) == 1 :
			return possible_answers[0]

		info_map = self.info_map # already updated when getting possible answers
		
		if self.rank_by == 'none' :
			if self.valid_guesses_only == True :
				return possible_answers[0]
			else :
				untried_words = five_letter_words-set(info_map.tried_words)
				return list(untried_words)[randrange(0, len(untried_words))]

		# Calculate score for possible guesses
		# Score types:
		# find_letter: How likely to reveal which letters are in the word, regardless of position
		# find_pos: How likely to reveal which letter is in each position
		word_scores = {}
		for word in five_letter_words :
			if word in info_map.tried_words:
				continue
			find_letter = 0
			for uniq_letter in set(word) :
				# Only care about letters we have not found yet
				if not uniq_letter in info_map.tried_letters :
					find_letter += word_letter_freq_ranked[uniq_letter]
			find_pos = 0

			for i, letter in enumerate(word) :
				# don't care about pos with known letters
				if i in info_map.pos_letter :
					continue
				# only count if letter has not been tried in the particular position before
				if (not any(tried_word.find(letter) == i for tried_word in info_map.tried_words)) :
					find_pos += pos_letter_freq_ranked[i][letter]
			
			word_scores[word] = { 
									'find_letter': find_letter, 
									'find_pos': find_pos, 
									'valid_answer': word in possible_answers 
								}

		# print(self.rank_by, self.valid_guesses_only)
		word_scores_ranked = sorted(word_scores.items(), key=lambda kv : kv[1][self.rank_by], reverse = True)
		if self.valid_guesses_only :
			word_scores_ranked = list(filter(lambda kv : kv[1]['valid_answer'] == True, word_scores_ranked))
		# for i, kv in enumerate(word_scores_ranked) :
		# 	if kv[0] == 'soths' :
		# 		print(i, kv)
		# print(word_scores_ranked[0:5])

		if len(word_scores_ranked) == 0 or word_scores_ranked[0][1][self.rank_by] == 0 :
			return possible_answers[0]

		return word_scores_ranked[0][0]


	def setStrategy(self, rank_by, valid_guesses_only) :
		self.rank_by = rank_by
		self.valid_guesses_only = valid_guesses_only


	def getStrategy(self) :
		return (self.rank_by, self.valid_guesses_only)


	def __init__(self) :
		self.setStrategy('find_letter', False)
		self.info_map = None
		self.word_scores = {}
		self.word_letter_freq = {}
		self.pos_letter_freq = {}
		fill_freq_maps()
